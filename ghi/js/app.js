function createCard(
  name,
  description,
  pictureUrl,
  startDate,
  endDate,
  location
) {
  return `
    <div class="card col-4 p-4 shadow">
        <img src="${pictureUrl}" class="card-img-top" alt="...">
        <div class="card-body">
            <h5 class="card-title">${name}</h5>
            <h6 class="card-subtitle text-muted mb-2">${location}</h6>
            <p class="card-text">${description}</p>
        </div>
        <div class="card-footer">${startDate} - ${endDate}</div>
    </div>
    `;
}

function formatDate(isoDate) {
  let date = new Date(Date.parse(isoDate));
  let month = date.getMonth();
  let day = date.getDay();
  let year = date.getFullYear();

  let formatted = `${month}/${day}/${year}`;

  return formatted;
}

window.addEventListener("DOMContentLoaded", async () => {
  const url = "http://localhost:8000/api/conferences/";

  try {
    const response = await fetch(url);
    if (!response.ok) {
    } else {
      const data = await response.json();
      //   const conference = data.conferences[0];

      //   const nameTag = document.querySelector(".card-title");
      //   nameTag.innerHTML = conference.name;

      for (let conference of data.conferences) {
        const detailUrl = `http://localhost:8000${conference.href}`;
        const detailResponse = await fetch(detailUrl);
        if (detailResponse.ok) {
          const details = await detailResponse.json();
          const name = details.conference.name;
          const description = details.conference.description;
          const pictureUrl = details.conference.location.picture_url;
          const location = details.conference.location.name;

          let startDate = details.conference.starts;
          startDate = formatDate(startDate);

          let endDate = details.conference.ends;
          endDate = formatDate(endDate);

          const html = createCard(
            name,
            description,
            pictureUrl,
            startDate,
            endDate,
            location
          );
          const row = document.querySelector(".row");
          row.innerHTML += html;
          // const column = document.querySelector(".col");
          // column.innerHTML += html;
        }
      }
    }
  } catch (e) {
    console.error(e);
  }
});
