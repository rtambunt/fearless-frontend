// Get the cookie out of the cookie store
const payloadCookie = await cookieStore.get("jwt_access_payload");
if (payloadCookie) {
  console.log(payloadCookie);
  // Convert the encoded payload from base64 to normal string
  const decodedPayload = atob(payloadCookie.value);

  // The cookie value is a JSON-formatted string, so parse it
  //   const encodedPayload = JSON.parse(decodedPayload);

  // The payload is a JSON-formatted string, so parse it
  const payload = JSON.parse(decodedPayload);
  // Print the payload
  console.log(payload);

  const userPermissions = payload.user.perms;

  // Check if "events.add_conference" is in the permissions.
  // If it is, remove 'd-none' from the link
  if (userPermissions.includes("events.add_conference")) {
    const conferenceLink = document.querySelector(".new-conference");
    conferenceLink.classList.remove("d-none");
  }

  // Check if "events.add_location" is in the permissions.
  // If it is, remove 'd-none' from the link
  if (userPermissions.includes("events.add_location")) {
    const locationLink = document.querySelector(".new-location");
    locationLink.classList.remove("d-none");
  }
}
