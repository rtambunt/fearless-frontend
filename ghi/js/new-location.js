window.addEventListener("DOMContentLoaded", async () => {
  const url = "http://localhost:8000/api/states/";
  const response = await fetch(url);

  if (response.ok) {
    const data = await response.json();
    console.log(data);

    const stateSelect = document.querySelector(".form-select");
    const states = data.states;

    for (let state of states) {
      const stateOption = document.createElement("option");
      stateOption.value = state.abbreviation;
      stateOption.innerHTML = state.name;
      stateSelect.appendChild(stateOption);
    }
  }

  // submit button functionality
  const formTag = document.getElementById("create-location-form");
  formTag.addEventListener("submit", async (e) => {
    e.preventDefault();

    const formData = new FormData(formTag);
    const json = JSON.stringify(Object.fromEntries(formData));

    const locationUrl = "http://localhost:8000/api/locations/";
    const fetchConfig = {
      method: "post",
      body: json,
      headers: {
        "Content-Type": "application/json",
      },
    };

    const response = await fetch(locationUrl, fetchConfig);
    if (response.ok) {
      formTag.reset();
      const newLocation = await response.json();
      console.log(newLocation);
    }
  });
});
