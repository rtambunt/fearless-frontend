window.addEventListener("DOMContentLoaded", async () => {
  const locationUrl = "http://localhost:8000/api/locations/";
  const locationResponse = await fetch(locationUrl);

  if (locationResponse.ok) {
    const locationData = await locationResponse.json();

    const selectLocation = document.querySelector(".form-select");
    const locations = locationData.locations;

    for (let location of locations) {
      const locationOption = document.createElement("option");
      locationOption.value = location.id;
      locationOption.innerHTML = location.name;
      selectLocation.appendChild(locationOption);
    }
  }

  // Making POST request
  const conferenceForm = document.getElementById("create-conference-form");
  conferenceForm.addEventListener("submit", async (e) => {
    e.preventDefault();

    const formData = new FormData(conferenceForm);
    const json = JSON.stringify(Object.fromEntries(formData));

    const conferenceUrl = "http://localhost:8000/api/conferences/";
    const fetchConfig = {
      method: "post",
      body: json,
      headers: { "Content-Type": "application/json" },
    };

    const response = await fetch(conferenceUrl, fetchConfig);
    if (response.ok) {
      conferenceForm.reset();
      const newConference = await response.json();
      console.log(newConference);
    }
  });
});
