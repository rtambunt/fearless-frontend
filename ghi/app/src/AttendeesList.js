import React from "react";

export const AttendeesList = ({ attendees }) => {
  return (
    <table>
      <thead>
        <tr>
          <th>Name</th>
          <th>Conference</th>
        </tr>
      </thead>
      <tbody>
        {attendees.map((attendee) => {
          return (
            <tr key={attendee.href}>
              <td>{attendee.name}</td>
              <td>{attendee.conference}</td>
            </tr>
          );
        })}
      </tbody>
    </table>
  );
};
